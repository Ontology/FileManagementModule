﻿using FileManagementModule.Factories;
using FileManagementModule.Models;
using FileManagementModule.Services;
using FileManagementModule.Translations;
using MediaStore_Module;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using OntoMsg_Module.Models;
using OntoMsg_Module.Notifications;
using OntoMsg_Module.StateMachines;
using OntoMsg_Module.WebSocketServices;
using OntoWebCore.Factories;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace FileManagementModule.Controllers
{
    public class FileDownUploadViewController : FileDownUploadViewModel, IViewController
    {
        private WebsocketServiceAgent webSocketServiceAgent;

        private TranslationController translationController = new TranslationController();

        private ServiceAgent_ElasticDownUpload serviceAgent_Elastic;

        private FilesFactory filesFactory;

        private clsLocalConfig localConfig;

        private JsonFactory jsonFactory;

        private List<FileItem> appliedItems = new List<FileItem>();

        private MediaStoreConnector mediaStoreConnector;

        private string dataFileName;

        public IControllerStateMachine StateMachine { get; private set; }
        public ControllerStateMachine LocalStateMachine
        {
            get
            {
                if (StateMachine == null)
                {
                    return null;
                }

                return (ControllerStateMachine)StateMachine;
            }
        }

        public FileDownUploadViewController()
        {
            // local configuration
            localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (localConfig == null)
            {
                localConfig = new clsLocalConfig(new Globals());
                LocalConfigManager.AddLocalConfig(localConfig);
            }

            Initialize();
            PropertyChanged += FileDownUploadViewController_PropertyChanged;
        }

        private void FileDownUploadViewController_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var property = ViewModelProperties.FirstOrDefault(viewProperty => viewProperty.Property.Name == e.PropertyName);

            if (property == null) return;

            property.ViewItem.AddValue(property.Property.GetValue(this));

            webSocketServiceAgent.SendPropertyChange(e.PropertyName);
        }

        private void Initialize()
        {
            var stateMachine = new ControllerStateMachine(StateMachineType.BlockSelectingSelected | StateMachineType.BlockLoadingSelected);
            stateMachine.IsControllerListen = true;
            StateMachine = stateMachine;
            stateMachine.PropertyChanged += StateMachine_PropertyChanged;
            stateMachine.loadSelectedItem += StateMachine_loadSelectedItem;
            stateMachine.loginSucceded += StateMachine_loginSucceded;
            stateMachine.openedSocket += StateMachine_openedSocket;
            stateMachine.closedSocket += StateMachine_closedSocket;

            serviceAgent_Elastic = new ServiceAgent_ElasticDownUpload(localConfig);
            serviceAgent_Elastic.PropertyChanged += ServiceAgent_Elastic_PropertyChanged;
            jsonFactory = new JsonFactory();
            filesFactory = new FilesFactory(localConfig);
            filesFactory.PropertyChanged += FilesFactory_PropertyChanged;
            mediaStoreConnector = new MediaStoreConnector(localConfig.Globals);
        }

        private void StateMachine_closedSocket()
        {
            webSocketServiceAgent.RemoveAllResources();
        }

        private void StateMachine_openedSocket()
        {
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Sender,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });
        }

        private void StateMachine_loginSucceded()
        {
            

            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                ChannelTypeId = Channels.ParameterList,
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Receiver,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });

            IsToggled_Listen = true;
            dataFileName = Guid.NewGuid().ToString() + ".json";


            ColumnConfig_Grid = GridFactory.CreateColumnList(typeof(FileItem));

            var sessionFile = webSocketServiceAgent.RequestWriteStream(dataFileName);

            if (sessionFile == null) return;

            var result = jsonFactory.CreateJsonFileOfItemList(typeof(FileItem), new List<object>(), sessionFile);

            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                JqxDataSource_Grid = GridFactory.CreateJqxDataSource(typeof(FileItem), null, null, sessionFile.FileUri.AbsoluteUri);
            }


            
            
        }

        private void StateMachine_loadSelectedItem(clsOntologyItem oItemSelected)
        {
            var result = serviceAgent_Elastic.GetFiles(oItemSelected);
        }

        private void StateMachine_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(ControllerStateMachine.LoginSuccessful))
            {
                IsEnabled_Download = false;
                
                IsSuccessful_Login = LocalStateMachine.LoginSuccessful;
                
            }
        }

        private void FilesFactory_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == Notifications.NotifyChanges.FilesFactory_FileItems)
            {
                if (filesFactory.FileItems != null)
                {
                    var fileItems = filesFactory.FileItems;
                    var sessionFile = webSocketServiceAgent.RequestWriteStream(dataFileName);
                    if (sessionFile != null)
                    {
                        var result = jsonFactory.CreateJsonFileOfItemList(typeof(FileItem), fileItems.ToList<object>(), sessionFile);
                        if (result.GUID == localConfig.Globals.LState_Success.GUID)
                        {
                            webSocketServiceAgent.SendCommand("reloadGrid");
                        }
                        
                    }
                }
            }
        }

        private void ServiceAgent_Elastic_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == Notifications.NotifyChanges.Elastic_ResultFileItems)
            {
                if (serviceAgent_Elastic.ResultFileItems.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    var result = filesFactory.BeginGetFileItems(serviceAgent_Elastic.FileItems,
                        serviceAgent_Elastic.RefToFileItems,
                        serviceAgent_Elastic.FilesToIsBlob);
                }
            }
        }

        public void InitializeViewController(WebsocketServiceAgent webSocketServiceAgent)
        {
            this.webSocketServiceAgent = webSocketServiceAgent;
            this.webSocketServiceAgent.PropertyChanged += WebSocketServiceAgent_PropertyChanged;
            this.webSocketServiceAgent.comServerOnMessage += WebSocketServiceAgent_comServerOnMessage;
            this.webSocketServiceAgent.comServerOpened += WebSocketServiceAgent_comServerOpened;


        }

        private void TogglePreAppliedRow(bool preApplied)
        {
            if (preApplied)
            {
                var idRow = webSocketServiceAgent.Request["IdRow"].ToString();
                var fileItem = filesFactory.FileItems.FirstOrDefault(fileItm => fileItm.IdRow == idRow);

                if (fileItem != null)
                {
                    if (!appliedItems.Any(applItm => applItm == fileItem))
                    {
                        appliedItems.Add(fileItem);
                    }
                }
            }
            else
            {
                var idRow = webSocketServiceAgent.Request["IdRow"].ToString();
                appliedItems.RemoveAll(fileItm => fileItm.IdRow == idRow);
            }

            ValidateDownloadButton();
        }

        private void WebSocketServiceAgent_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_DataText_SessionId)
            {


            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Command_RequestedCommand)
            {
                if (webSocketServiceAgent.Command_RequestedCommand == "PreAppliedRow")
                {
                    TogglePreAppliedRow(true);
                }
                else if (webSocketServiceAgent.Command_RequestedCommand == "UnPreAppliedRow")
                {
                    TogglePreAppliedRow(false);
                }
                else if (webSocketServiceAgent.Command_RequestedCommand == "DownloadFiles")
                {
                    var filesToDownload = appliedItems.GroupBy(applItm => new { GUID = applItm.IdFile, Name = applItm.NameFile }).Select(fileItm => new clsOntologyItem
                    {
                        GUID = fileItm.Key.GUID,
                        Name = fileItm.Key.Name,
                        GUID_Parent = localConfig.OItem_type_file.GUID,
                        Type = localConfig.Globals.Type_Object
                    }).ToList();

                    var filesCountToDo = filesToDownload.Count;
                    var filesCountDone = 0;

                    var downloadFileName = "download_" + Guid.NewGuid().ToString() + ".zip";
                    var sessionFile = webSocketServiceAgent.RequestWriteStream(downloadFileName);

                    if (sessionFile != null)
                    {
                        

                        try
                        {
                            using (sessionFile.StreamWriter)
                            {
                                using (var zipArchive = new ZipArchive(sessionFile.StreamWriter.BaseStream, ZipArchiveMode.Create))
                                {
                                    filesToDownload.ForEach(file =>
                                    {
                                        var blobStream = mediaStoreConnector.GetManagedMediaStream(file,true);

                                        if (blobStream != null)
                                        {
                                            var zipArchiveEntry = zipArchive.CreateEntry(file.Name, CompressionLevel.Optimal);
                                            using (var writeStream = zipArchiveEntry.Open())
                                            {
                                                blobStream.CopyTo(writeStream);
                                                filesCountDone++;
                                            }



                                        }

                                    });

                                }
                            }
                        }
                        catch(Exception ex)
                        {

                        }

                        if (filesCountToDo == filesCountDone)
                        {
                            Url_Download = sessionFile.FileUri.AbsoluteUri;
                        }
                    }
                }

            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Event_RequestEvent)
            {

            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_ChangedViewItems)
            {
                webSocketServiceAgent.ChangedViewItems.ForEach(changedItem =>
                {

                });
            }
            else if (e.PropertyName == NotifyChanges.Websocket_ObjectArgument)
            {
                var objectIdItem = webSocketServiceAgent.ObjectArgument?.Value;
                string objectId = objectIdItem?.ToString();

                if (string.IsNullOrEmpty(objectId) && !localConfig.Globals.is_GUID(objectId)) return;

                var oItem = serviceAgent_Elastic.GetOItem(objectId, localConfig.Globals.Type_Object);

                LocalStateMachine.SetItemSelected(oItem);

                
                
            }



        }

        private void WebSocketServiceAgent_comServerOpened()
        {
            var authenticationRequest = new InterServiceMessage
            {
                ChannelId = Channels.Login,
                SenderId = webSocketServiceAgent.EndpointId
            };

            webSocketServiceAgent.SendInterModMessage(authenticationRequest);

        }

        private void WebSocketServiceAgent_comServerOnMessage(OntoMsg_Module.Notifications.InterServiceMessage message)
        {

            if (message.ReceiverId != null && message.ReceiverId != webSocketServiceAgent.EndpointId) return;

            if (message.ChannelId == Channels.ReceiverOfDedicatedSender)
            {
            }
            else if (message.ChannelId == Channels.ParameterList)
            {
                var oItem = message.OItems.FirstOrDefault();

                if (oItem == null) return;

                var selectedRef = serviceAgent_Elastic.GetOItem(oItem.GUID, localConfig.Globals.Type_Object);

                LocalStateMachine.SetItemSelected(selectedRef);
            }
            
        }


        public List<ViewModelProperty> GetViewModelProperties(bool onlySend = true, ViewItemType viewItemType = ViewItemType.All, ViewItemClass viewItemClass = ViewItemClass.All)
        {
            return ViewModelProperties.Where(viewItemProp => (onlySend ? viewItemProp.ViewModelAttribute.Send : 1 == 1)
                && (viewItemType != ViewItemType.All ? viewItemProp.ViewModelAttribute.ViewItemType == viewItemType : 1 == 1)
                && (viewItemClass != ViewItemClass.All ? viewItemProp.ViewModelAttribute.ViewItemClass == viewItemClass : 1 == 1)).ToList();
        }

        private void ValidateDownloadButton()
        {
            IsEnabled_Download = appliedItems.Any() && appliedItems.All(appliedItem => appliedItem.IsBlob);
        }
    }
}

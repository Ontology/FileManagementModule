﻿using FileManagementModule.Notifications;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileManagementModule.Controllers
{
    public class FileDownUploadViewModel : ViewModelBase
    {
        private bool issuccessful_Login;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "loginSuccess", ViewItemType = ViewItemType.Other)]
		public bool IsSuccessful_Login
        {
            get { return issuccessful_Login; }
            set
            {
                if (issuccessful_Login == value) return;

                issuccessful_Login = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsSuccessful_Login);

            }
        }

        private bool istoggled_Listen;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "isListen", ViewItemType = ViewItemType.Other)]
		public bool IsToggled_Listen
        {
            get { return istoggled_Listen; }
            set
            {
                if (istoggled_Listen == value) return;

                istoggled_Listen = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsToggled_Listen);

            }
        }

        private string text_View;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "viewText", ViewItemType = ViewItemType.Other)]
		public string Text_View
        {
            get { return text_View; }
            set
            {
                if (text_View == value) return;

                text_View = value;

                RaisePropertyChanged(NotifyChanges.View_Text_View);

            }
        }

        private JqxColumnList columnconfig_Grid;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Grid, ViewItemId = "grid", ViewItemType = ViewItemType.ColumnConfig)]
		public JqxColumnList ColumnConfig_Grid
        {
            get { return columnconfig_Grid; }
            set
            {
                if (columnconfig_Grid == value) return;

                columnconfig_Grid = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_ColumnConfig_Grid);

            }
        }

        private JqxDataSource jqxdatasource_Grid;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Grid, ViewItemId = "grid", ViewItemType = ViewItemType.DataSource)]
		public JqxDataSource JqxDataSource_Grid
        {
            get { return jqxdatasource_Grid; }
            set
            {
                if (jqxdatasource_Grid == value) return;

                jqxdatasource_Grid = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_JqxDataSource_Grid);

            }
        }

        private bool isenabled_Download;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemId = "downloadFiles", ViewItemType = ViewItemType.Enable)]
        public bool IsEnabled_Download
        {
            get { return isenabled_Download; }
            set
            {

                isenabled_Download = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_Download);

            }
        }

        private string url_Download;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Url, ViewItemId = "downloadUrl", ViewItemType = ViewItemType.Content)]
		public string Url_Download
        {
            get { return url_Download; }
            set
            {
                if (url_Download == value) return;

                url_Download = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Url_Download);

            }
        }
    }
}

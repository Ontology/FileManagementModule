﻿using FileManagementModule.Models;
using FileManagementModule.Notifications;
using ImportExport_Module.Base;
using OntologyClasses.BaseClasses;
using OntologyClasses.DataClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MediaStore_Module;

namespace FileManagementModule.Factories
{
    public class FilesFactory : NotifyPropertyChange
    {
        private clsLocalConfig localConfig;

        private FileWorkManager fileWorkManager;

        private object asyncLocker = new object();

        private List<FileItem> fileItems;
        public List<FileItem> FileItems
        {
            get
            {
                lock(asyncLocker)
                {
                    return fileItems;
                }
                
            }
            set
            {
                lock (asyncLocker)
                {
                    fileItems = value;
                }
                RaisePropertyChanged(NotifyChanges.FilesFactory_FileItems);
            }
        }

        private Thread getFileItemsAsync;

        public void StopRead()
        {
            StopReadGetFileItems();
        }

        private void StopReadGetFileItems()
        {
            if (getFileItemsAsync != null)
            {
                try
                {
                    getFileItemsAsync.Abort();
                }
                catch(Exception ex)
                {

                }
            }
        }

        public clsOntologyItem BeginGetFileItems(List<clsOntologyItem> fileItems,
            List<clsObjectRel> refToFiles,
            List<clsObjectAtt> filesToIsBlob)
        {
            StopReadGetFileItems();

            var paramItem = new ThreadParam
            {
                Files = fileItems,
                RefToFiles = refToFiles,
                FilesToIsBlob = filesToIsBlob
            };

            getFileItemsAsync = new Thread(GetFileItemsAsync);
            getFileItemsAsync.Start(paramItem);

            return localConfig.Globals.LState_Success.Clone();
        }

        private void GetFileItemsAsync(object paramItem)
        {
            var dataItems = (ThreadParam)paramItem;

            var fileItems = new List<FileItem>();

            if (dataItems.Files != null)
            {
                fileItems = (from file in dataItems.Files
                             join isBlob in dataItems.FilesToIsBlob on file.GUID equals isBlob.ID_Object into isBlobs
                             from isBlob in isBlobs.DefaultIfEmpty()
                             select new FileItem
                             {
                                 IdFile = file.GUID,
                                 NameFile = file.Name,
                                 IdRow = Guid.NewGuid().ToString(),
                                 OrderId = 0,
                                 IsBlob = isBlob != null ? isBlob.Val_Bit.Value : false,
                                 Path = fileWorkManager.GetPathFileSystemObject(file)
                             }).ToList();
            }
            else
            {
                var refToFiles = dataItems.RefToFiles.Where(refToFile => refToFile.ID_Parent_Object == localConfig.OItem_type_file.GUID);
                var filesToRef = dataItems.RefToFiles.Where(refToFile => refToFile.ID_Parent_Other == localConfig.OItem_type_file.GUID);
                if (refToFiles.Any())
                {
                    fileItems.AddRange(from refToFile in refToFiles
                                       join isBlob in dataItems.FilesToIsBlob on refToFile.ID_Object equals isBlob.ID_Object into isBlobs
                                       from isBlob in isBlobs.DefaultIfEmpty()
                                       select new FileItem
                                       {
                                           IdFile = refToFile.ID_Object,
                                           NameFile = refToFile.Name_Object,
                                           IdRelationType = refToFile.ID_RelationType,
                                           NameRelationType = refToFile.Name_RelationType,
                                           IdRef = refToFile.ID_Other,
                                           NameRef = refToFile.Name_Other,
                                           IdRow = Guid.NewGuid().ToString(),
                                           OrderId = 0,
                                           IsBlob = isBlob != null ? isBlob.Val_Bit.Value : false,
                                           Path = fileWorkManager.GetPathFileSystemObject(new clsOntologyItem
                                           {
                                               GUID = refToFile.ID_Object,
                                               Name = refToFile.Name_Object,
                                               GUID_Parent = refToFile.ID_Parent_Object,
                                               Type = localConfig.Globals.Type_Object
                                           })
                                       });
                }
                if (filesToRef.Any())
                {
                    fileItems.AddRange(from fileToRef in filesToRef
                                       join isBlob in dataItems.FilesToIsBlob on fileToRef.ID_Other equals isBlob.ID_Object into isBlobs
                                       from isBlob in isBlobs.DefaultIfEmpty()
                                       select new FileItem
                                       {
                                           IdFile = fileToRef.ID_Other,
                                           NameFile = fileToRef.Name_Other,
                                           IdRelationType = fileToRef.ID_RelationType,
                                           NameRelationType = fileToRef.Name_RelationType,
                                           IdRef = fileToRef.ID_Other,
                                           NameRef = fileToRef.Name_Other,
                                           IdRow = Guid.NewGuid().ToString(),
                                           OrderId = 0,
                                           IsBlob = isBlob != null ? isBlob.Val_Bit.Value : false,
                                           Path = fileWorkManager.GetPathFileSystemObject(new clsOntologyItem
                                           {
                                               GUID = fileToRef.ID_Other,
                                               Name = fileToRef.Name_Other,
                                               GUID_Parent = fileToRef.ID_Parent_Other,
                                               Type = localConfig.Globals.Type_Object
                                           })
                                       });
                }
            }

            FileItems = fileItems;
        }

        public FilesFactory(clsLocalConfig localConfig)
        {
            this.localConfig = localConfig;
            Initialize();
        }

        private void Initialize()
        {
            fileWorkManager = new FileWorkManager(localConfig.Globals);
        }
    }

    class ThreadParam
    {
        public List<clsOntologyItem> Files { get; set; }
        public List<clsObjectRel> RefToFiles { get; set; }
        public List<clsObjectAtt> FilesToIsBlob { get; set; }
    }
}

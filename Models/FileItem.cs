﻿using FileManagementModule.Notifications;
using ImportExport_Module.Base;
using OntoWebCore.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileManagementModule.Models
{
    public class FileItem : NotifyPropertyChange
    {
        private string idRow;
		[DataViewColumn(CellType = CellType.String, IsIdField = true, IsVisible = false)]
        [Json()]
        public string IdRow
        {
            get { return idRow; }
            set
            {
                if (idRow == value) return;

                idRow = value;

                RaisePropertyChanged(NotifyChanges.DataModel_IdRow);

            }
        }

        private bool apply;
		[DataViewColumn(Caption = "Apply", CellType = CellType.Boolean, DisplayOrder = 0, IsIdField = false, IsVisible = true, IsReadonly = false)]
        [Json()]
        public bool Apply
        {
            get { return apply; }
            set
            {
                if (apply == value) return;

                apply = value;

                RaisePropertyChanged(NotifyChanges.DataModel_Apply);

            }
        }

        private long orderId;
		[DataViewColumn(Caption = "Order-Id", CellType = CellType.Integer, DisplayOrder = 8, IsIdField = false, IsVisible = true)]
        [Json()]
        public long OrderId
        {
            get { return orderId; }
            set
            {
                if (orderId == value) return;

                orderId = value;

                RaisePropertyChanged(NotifyChanges.DataModel_OrderId);

            }
        }

        private string idFile;
		[DataViewColumn(IsIdField = false, IsVisible = false)]
        [Json()]
        public string IdFile
        {
            get { return idFile; }
            set
            {
                if (idFile == value) return;

                idFile = value;

                RaisePropertyChanged(NotifyChanges.DataModel_IdFile);

            }
        }

        private string nameFile;
		[DataViewColumn(Caption = "Filename", CellType = CellType.String, DisplayOrder = 1, IsIdField = false, IsVisible = true)]
        [Json()]
        public string NameFile
        {
            get { return nameFile; }
            set
            {
                if (nameFile == value) return;

                nameFile = value;

                RaisePropertyChanged(NotifyChanges.DataModel_NameFile);

            }
        }

        private string idRelationType;
		[DataViewColumn(CellType = CellType.String, IsIdField = false, IsVisible = false)]
        [Json()]
        public string IdRelationType
        {
            get { return idRelationType; }
            set
            {
                if (idRelationType == value) return;

                idRelationType = value;

                RaisePropertyChanged(NotifyChanges.DataModel_IdRelationType);

            }
        }

        private string nameRelationType;
		[DataViewColumn(Caption = "Relationtype", CellType = CellType.String, DisplayOrder = 2, IsIdField = false, IsVisible = true)]
        [Json()]
        public string NameRelationType
        {
            get { return nameRelationType; }
            set
            {
                if (nameRelationType == value) return;

                nameRelationType = value;

                RaisePropertyChanged(NotifyChanges.DataModel_NameRelationType);

            }
        }

        private string nameRef;
		[DataViewColumn(CellType = CellType.String, DisplayOrder = 3, IsIdField = false, IsVisible = true)]
        public string NameRef
        {
            get { return nameRef; }
            set
            {
                if (nameRef == value) return;

                nameRef = value;

                RaisePropertyChanged(NotifyChanges.DataModel_NameRef);

            }
        }

        private string idRef;
		[DataViewColumn(CellType = CellType.String, IsIdField = false, IsVisible = false)]
        public string IdRef
        {
            get { return idRef; }
            set
            {
                if (idRef == value) return;

                idRef = value;

                RaisePropertyChanged(NotifyChanges.DataModel_IdRef);

            }
        }

        private bool isBlob;
		[DataViewColumn(Caption = "Managed", CellType = CellType.Boolean, DisplayOrder = 3, IsIdField = false, IsVisible = true)]
        [Json()]
        public bool IsBlob
        {
            get { return isBlob; }
            set
            {
                if (isBlob == value) return;

                isBlob = value;

                RaisePropertyChanged(NotifyChanges.DataModel_IsBlob);

            }
        }

        private string path;
		[DataViewColumn(Caption = "Path", CellType = CellType.String, DisplayOrder = 4, IsIdField = false, IsVisible = true)]
        [Json()]
        public string Path
        {
            get { return path; }
            set
            {
                if (path == value) return;

                path = value;

                RaisePropertyChanged(NotifyChanges.DataModel_Path);

            }
        }
    }
}

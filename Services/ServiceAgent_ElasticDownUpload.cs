﻿using FileManagementModule.Notifications;
using ImportExport_Module.Base;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FileManagementModule.Services
{
    public class ServiceAgent_ElasticDownUpload : NotifyPropertyChange
    {

        private clsLocalConfig localConfig;

        private object asyncLocker = new object();

        private clsOntologyItem resultFileItems;
        public clsOntologyItem ResultFileItems
        {
            get
            {
                lock(asyncLocker)
                {
                    return resultFileItems;
                }

            }
            set
            {
                lock(asyncLocker)
                {
                    resultFileItems = value;
                }
                
                RaisePropertyChanged(NotifyChanges.Elastic_ResultFileItems);
            }
        }


        private List<clsObjectRel> refToFileItems;
        public List<clsObjectRel> RefToFileItems
        {
            get
            {
                lock (asyncLocker)
                {
                    return refToFileItems;
                }
            }
            set
            {
                lock (asyncLocker)
                {
                    refToFileItems = value;
                }
                RaisePropertyChanged(NotifyChanges.Elastic_RefToFileItems);
            }
        }

        private List<clsOntologyItem> fileItems;
        public List<clsOntologyItem> FileItems
        {
            get
            {
                lock (asyncLocker)
                {
                    return fileItems;
                }
            }
            set
            {
                lock (asyncLocker)
                {
                    fileItems = value;
                }
                RaisePropertyChanged(NotifyChanges.Elastic_FileItems);
            }

        }

        private List<clsObjectAtt> filesToIsBlob;
        public List<clsObjectAtt> FilesToIsBlob
        {
            get
            {
                lock (asyncLocker)
                {
                    return filesToIsBlob;
                }
            }
            set
            {
                lock (asyncLocker)
                {
                    filesToIsBlob = value;
                }
                RaisePropertyChanged(NotifyChanges.Elastic_FilesToIsBlob);
            }
        }

        private Thread getFilesAsync;

        public clsOntologyItem GetOItem(string id, string type)
        {
            var dbReader = new OntologyModDBConnector(localConfig.Globals);

            return dbReader.GetOItem(id, type);
        }

        public void StopRead()
        {
            StopReadGetFiles();
        }

        private void StopReadGetFiles()
        {
            if (getFilesAsync != null)
            {
                getFilesAsync.Abort();
            }
        }

        public clsOntologyItem GetFiles(clsOntologyItem oItemRef)
        {
            StopReadGetFiles();

            getFilesAsync = new Thread(GetFilesAsync);
            getFilesAsync.Start(oItemRef);

            return localConfig.Globals.LState_Success.Clone();
        }

        public void GetFilesAsync(object item)
        {
            var oItemRef = (clsOntologyItem)item;
            var result = localConfig.Globals.LState_Success.Clone();
            if (oItemRef.GUID_Parent == localConfig.OItem_type_file.GUID)
            {
                var oItemFile = GetOItem(oItemRef.GUID, localConfig.Globals.Type_Object);

                if (oItemFile == null)
                {
                    
                    ResultFileItems = localConfig.Globals.LState_Error.Clone();
                    return;
                }

                FileItems = new List<clsOntologyItem> { oItemFile };
            }
            else
            {
                result = GetFilesOfRef(oItemRef);
            }

            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                ResultFileItems = result;
                return;
            }

            result = GetIsBlobOfFiles(FileItems, RefToFileItems);

            ResultFileItems = result;
        }

        public clsOntologyItem GetFilesOfRef(clsOntologyItem oItemRef)
        {
            var dbReaderLeftRight = new OntologyModDBConnector(localConfig.Globals);
            var dbReaderRightLeft = new OntologyModDBConnector(localConfig.Globals);

            var searchFilesLeftRight = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Object = oItemRef.GUID,
                    ID_Parent_Other = localConfig.OItem_type_file.GUID
                }
            };

            var result = dbReaderLeftRight.GetDataObjectRel(searchFilesLeftRight);
            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                RefToFileItems = null;
                return result;
            }

            var searchFilesRightLeft = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Other = oItemRef.GUID,
                    ID_Parent_Object = localConfig.OItem_type_file.GUID
                }
            };

            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {

                var refToFileItems = dbReaderLeftRight.ObjectRels;
                refToFileItems.AddRange(dbReaderRightLeft.ObjectRels);
                RefToFileItems = refToFileItems;
            }
            else
            {
                RefToFileItems = null;
            }

            return result;
        }

        public clsOntologyItem GetIsBlobOfFiles(List<clsOntologyItem> fileItems, List<clsObjectRel> filesOfRef)
        {
            var dbReader = new OntologyModDBConnector(localConfig.Globals);
            var searchFilesToIsBlob = new List<clsObjectAtt>();
            var result = localConfig.Globals.LState_Success.Clone();

            if (fileItems != null)
            {
                searchFilesToIsBlob = fileItems.Select(fileItem => new clsObjectAtt
                {
                    ID_Object = fileItem.GUID,
                    ID_AttributeType = localConfig.OItem_attribute_blob.GUID
                }).ToList();
                if (searchFilesToIsBlob.Any())
                {
                    result = dbReader.GetDataObjectAtt(searchFilesToIsBlob);

                    if (result.GUID == localConfig.Globals.LState_Success.GUID)
                    {
                        FilesToIsBlob = dbReader.ObjAtts;
                    }
                    else
                    {
                        FilesToIsBlob = null;
                    }
                }
                else
                {
                    FilesToIsBlob = new List<clsObjectAtt>();
                }
                
            }
            else
            {
                searchFilesToIsBlob = filesOfRef.Where(fileItm => fileItm.ID_Parent_Object == localConfig.OItem_type_file.GUID).Select(fileOfRef => new clsObjectAtt
                {
                    ID_Object = fileOfRef.ID_Object,
                    ID_AttributeType = localConfig.OItem_attribute_blob.GUID
                }).ToList();

                if (searchFilesToIsBlob.Any())
                {
                    result = dbReader.GetDataObjectAtt(searchFilesToIsBlob);

                    if (result.GUID == localConfig.Globals.LState_Success.GUID)
                    {
                        if (dbReader.ObjAtts.Any())
                        {
                            FilesToIsBlob = dbReader.ObjAtts;
                        }
                        else
                        {
                            FilesToIsBlob = new List<clsObjectAtt>();

                        }

                    }
                    else
                    {
                        FilesToIsBlob = null;
                    }
                }
                else
                {
                    searchFilesToIsBlob = filesOfRef.Where(fileItm => fileItm.ID_Parent_Other == localConfig.OItem_type_file.GUID).Select(fileOfRef => new clsObjectAtt
                    {
                        ID_Object = fileOfRef.ID_Other,
                        ID_AttributeType = localConfig.OItem_attribute_blob.GUID
                    }).ToList();

                    if (searchFilesToIsBlob.Any())
                    {
                        result = dbReader.GetDataObjectAtt(searchFilesToIsBlob);

                        if (result.GUID == localConfig.Globals.LState_Success.GUID)
                        {
                            FilesToIsBlob = dbReader.ObjAtts;
                        }
                        else
                        {
                            FilesToIsBlob = null;
                        }
                    }
                    else
                    {
                        FilesToIsBlob = new List<clsObjectAtt>();
                    }
                    
                }
                
            }

            

            return result;
        }

        public ServiceAgent_ElasticDownUpload(clsLocalConfig localConfig)
        {
            this.localConfig = localConfig;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OntologyAppDBConnector;
using ImportExport_Module;
using OntologyClasses.BaseClasses;
using System.Reflection;
using OntologyClasses.Interfaces;
using System.Runtime.InteropServices;

namespace FileManagementModule
{
    public class clsLocalConfig : ILocalConfig
    {
    private const string cstrID_Ontology = "6afbdb4bfecb40ca9aa73070e209e31e";
    private ImportWorker objImport;

    public Globals Globals { get; set; }

    private clsOntologyItem objOItem_DevConfig = new clsOntologyItem();
    public clsOntologyItem OItem_BaseConfig { get; set; }

    private OntologyModDBConnector objDBLevel_Config1;
    private OntologyModDBConnector objDBLevel_Config2;

    public clsOntologyItem OItem_attribute_blob { get; set; }
    public clsOntologyItem OItem_attribute_datetimestamp__create_ { get; set; }
    public clsOntologyItem OItem_attribute_dbpostfix { get; set; }
    public clsOntologyItem OItem_attribute_hash { get; set; }
    public clsOntologyItem OItem_attribute_path { get; set; }
    public clsOntologyItem OItem_attributetype_datetimestamp { get; set; }
    public clsOntologyItem OItem_attributetype_pattern { get; set; }
    public clsOntologyItem OItem_attributetype_subitems { get; set; }
    public clsOntologyItem OItem_class_blobsyncdirection { get; set; }
    public clsOntologyItem OItem_class_fileresource { get; set; }
    public clsOntologyItem OItem_class_filesync { get; set; }
    public clsOntologyItem OItem_class_logentry { get; set; }
    public clsOntologyItem OItem_class_logstate { get; set; }
    public clsOntologyItem OItem_class_password { get; set; }
    public clsOntologyItem OItem_class_url { get; set; }
    public clsOntologyItem OItem_class_user { get; set; }
    public clsOntologyItem OItem_class_user_authentication { get; set; }
    public clsOntologyItem OItem_class_web_connection { get; set; }
    public clsOntologyItem OItem_object_blob_to_file { get; set; }
    public clsOntologyItem OItem_object_create { get; set; }
    public clsOntologyItem OItem_object_download { get; set; }
    public clsOntologyItem OItem_object_file_has_no_identity { get; set; }
    public clsOntologyItem OItem_object_file_not_present { get; set; }
    public clsOntologyItem OItem_object_file_to_blob { get; set; }
    public clsOntologyItem OItem_object_identity_is_no_guid { get; set; }
    public clsOntologyItem OItem_object_identitypresent { get; set; }
    public clsOntologyItem OItem_relationtype_authorized_by { get; set; }
    public clsOntologyItem OItem_relationtype_belonging { get; set; }
    public clsOntologyItem OItem_relationtype_belonging_done { get; set; }
    public clsOntologyItem OItem_relationtype_belonging_resource { get; set; }
    public clsOntologyItem OItem_relationtype_belonging_source { get; set; }
    public clsOntologyItem OItem_relationtype_belongsto { get; set; }
    public clsOntologyItem OItem_relationtype_connect_to { get; set; }
    public clsOntologyItem OItem_relationtype_dst { get; set; }
    public clsOntologyItem OItem_relationtype_ends_with { get; set; }
    public clsOntologyItem OItem_relationtype_fileshare { get; set; }
    public clsOntologyItem OItem_relationtype_is_checkout_by { get; set; }
    public clsOntologyItem OItem_relationtype_is_of_type { get; set; }
    public clsOntologyItem OItem_relationtype_isinstate { get; set; }
    public clsOntologyItem OItem_relationtype_issubordinated { get; set; }
    public clsOntologyItem OItem_relationtype_last_done { get; set; }
    public clsOntologyItem OItem_relationtype_located_in { get; set; }
    public clsOntologyItem OItem_relationtype_offered_by { get; set; }
    public clsOntologyItem OItem_relationtype_provides { get; set; }
    public clsOntologyItem OItem_relationtype_secured_by { get; set; }
    public clsOntologyItem OItem_relationtype_src { get; set; }
    public clsOntologyItem OItem_relationtype_watch { get; set; }
    public clsOntologyItem OItem_token_active_server_state { get; set; }
    public clsOntologyItem OItem_token_fileserver_server_type { get; set; }
    public clsOntologyItem OItem_token_logstate_active { get; set; }
    public clsOntologyItem OItem_token_module_filesystem_management { get; set; }
    public clsOntologyItem OItem_type_blobdirwatcher { get; set; }
    public clsOntologyItem OItem_type_database { get; set; }
    public clsOntologyItem OItem_type_database_on_server { get; set; }
    public clsOntologyItem OItem_type_drive { get; set; }
    public clsOntologyItem OItem_type_extensions { get; set; }
    public clsOntologyItem OItem_type_file { get; set; }
    public clsOntologyItem OItem_type_filesystem_management { get; set; }
    public clsOntologyItem OItem_type_folder { get; set; }
    public clsOntologyItem OItem_type_module { get; set; }
    public clsOntologyItem OItem_type_path { get; set; }
    public clsOntologyItem OItem_type_server { get; set; }
    public clsOntologyItem OItem_type_server_state { get; set; }
    public clsOntologyItem OItem_type_server_type { get; set; }



    private void get_Data_DevelopmentConfig()
    {
        var objORL_Ontology_To_OntolgyItems = new List<clsObjectRel> {new clsObjectRel {ID_Object = cstrID_Ontology,
                                                                                             ID_RelationType = Globals.RelationType_contains.GUID,
                                                                                             ID_Parent_Other = Globals.Class_OntologyItems.GUID}};

        var objOItem_Result = objDBLevel_Config1.GetDataObjectRel(objORL_Ontology_To_OntolgyItems, doIds: false);
        if (objOItem_Result.GUID == Globals.LState_Success.GUID)
        {
            if (objDBLevel_Config1.ObjectRels.Any())
            {

                objORL_Ontology_To_OntolgyItems = objDBLevel_Config1.ObjectRels.Select(oi => new clsObjectRel
                {
                    ID_Object = oi.ID_Other,
                    ID_RelationType = Globals.RelationType_belongingAttribute.GUID
                }).ToList();

                objORL_Ontology_To_OntolgyItems.AddRange(objDBLevel_Config1.ObjectRels.Select(oi => new clsObjectRel
                {
                    ID_Object = oi.ID_Other,
                    ID_RelationType = Globals.RelationType_belongingClass.GUID
                }));
                objORL_Ontology_To_OntolgyItems.AddRange(objDBLevel_Config1.ObjectRels.Select(oi => new clsObjectRel
                {
                    ID_Object = oi.ID_Other,
                    ID_RelationType = Globals.RelationType_belongingObject.GUID
                }));
                objORL_Ontology_To_OntolgyItems.AddRange(objDBLevel_Config1.ObjectRels.Select(oi => new clsObjectRel
                {
                    ID_Object = oi.ID_Other,
                    ID_RelationType = Globals.RelationType_belongingRelationType.GUID
                }));

                objOItem_Result = objDBLevel_Config2.GetDataObjectRel(objORL_Ontology_To_OntolgyItems, doIds: false);
                if (objOItem_Result.GUID == Globals.LState_Success.GUID)
                {
                    if (!objDBLevel_Config2.ObjectRels.Any())
                    {
                        throw new Exception("Config-Error");
                    }
                }
                else
                {
                    throw new Exception("Config-Error");
                }
            }
            else
            {
                throw new Exception("Config-Error");
            }

        }

    }

    public clsLocalConfig()
    {
        Globals = new Globals();
        set_DBConnection();
        get_Config();
    }

    public clsLocalConfig(Globals Globals)
    {
        this.Globals = Globals;
        set_DBConnection();
        get_Config();
    }

    private void set_DBConnection()
    {
        objDBLevel_Config1 = new OntologyModDBConnector(Globals);
        objDBLevel_Config2 = new OntologyModDBConnector(Globals);
        objImport = new ImportWorker(Globals);
    }

    private void get_Config()
    {
        try
        {
            get_Data_DevelopmentConfig();
            get_Config_AttributeTypes();
            get_Config_RelationTypes();
            get_Config_Classes();
            get_Config_Objects();
        }
        catch (Exception ex)
        {
            var objAssembly = Assembly.GetExecutingAssembly();
            AssemblyTitleAttribute[] objCustomAttributes = (AssemblyTitleAttribute[])objAssembly.GetCustomAttributes(typeof(AssemblyTitleAttribute), false);
            var strTitle = "Unbekannt";
            if (objCustomAttributes.Length == 1)
            {
                strTitle = objCustomAttributes.First().Title;
            }
            
            var objOItem_Result = objImport.ImportTemplates(objAssembly);
            if (objOItem_Result.GUID != Globals.LState_Error.GUID)
            {
                get_Data_DevelopmentConfig();
                get_Config_AttributeTypes();
                get_Config_RelationTypes();
                get_Config_Classes();
                get_Config_Objects();
            }
            else
            {
                throw new Exception("Config not importable");
            }
            
        }
    }

    private void get_Config_AttributeTypes()
    {
        var objOList_attribute_blob = (from objOItem in objDBLevel_Config1.ObjectRels
                                       where objOItem.ID_Object == cstrID_Ontology
                                       join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                       where objRef.Name_Object.ToLower() == "attribute_blob".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                       select objRef).ToList();

        if (objOList_attribute_blob.Any())
        {
            OItem_attribute_blob = new clsOntologyItem()
            {
                GUID = objOList_attribute_blob.First().ID_Other,
                Name = objOList_attribute_blob.First().Name_Other,
                GUID_Parent = objOList_attribute_blob.First().ID_Parent_Other,
                Type = Globals.Type_AttributeType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_attribute_datetimestamp__create_ = (from objOItem in objDBLevel_Config1.ObjectRels
                                                         where objOItem.ID_Object == cstrID_Ontology
                                                         join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                         where objRef.Name_Object.ToLower() == "attribute_datetimestamp__create_".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                                         select objRef).ToList();

        if (objOList_attribute_datetimestamp__create_.Any())
        {
            OItem_attribute_datetimestamp__create_ = new clsOntologyItem()
            {
                GUID = objOList_attribute_datetimestamp__create_.First().ID_Other,
                Name = objOList_attribute_datetimestamp__create_.First().Name_Other,
                GUID_Parent = objOList_attribute_datetimestamp__create_.First().ID_Parent_Other,
                Type = Globals.Type_AttributeType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_attribute_dbpostfix = (from objOItem in objDBLevel_Config1.ObjectRels
                                            where objOItem.ID_Object == cstrID_Ontology
                                            join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                            where objRef.Name_Object.ToLower() == "attribute_dbpostfix".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                            select objRef).ToList();

        if (objOList_attribute_dbpostfix.Any())
        {
            OItem_attribute_dbpostfix = new clsOntologyItem()
            {
                GUID = objOList_attribute_dbpostfix.First().ID_Other,
                Name = objOList_attribute_dbpostfix.First().Name_Other,
                GUID_Parent = objOList_attribute_dbpostfix.First().ID_Parent_Other,
                Type = Globals.Type_AttributeType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_attribute_hash = (from objOItem in objDBLevel_Config1.ObjectRels
                                       where objOItem.ID_Object == cstrID_Ontology
                                       join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                       where objRef.Name_Object.ToLower() == "attribute_hash".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                       select objRef).ToList();

        if (objOList_attribute_hash.Any())
        {
            OItem_attribute_hash = new clsOntologyItem()
            {
                GUID = objOList_attribute_hash.First().ID_Other,
                Name = objOList_attribute_hash.First().Name_Other,
                GUID_Parent = objOList_attribute_hash.First().ID_Parent_Other,
                Type = Globals.Type_AttributeType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_attribute_path = (from objOItem in objDBLevel_Config1.ObjectRels
                                       where objOItem.ID_Object == cstrID_Ontology
                                       join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                       where objRef.Name_Object.ToLower() == "attribute_path".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                       select objRef).ToList();

        if (objOList_attribute_path.Any())
        {
            OItem_attribute_path = new clsOntologyItem()
            {
                GUID = objOList_attribute_path.First().ID_Other,
                Name = objOList_attribute_path.First().Name_Other,
                GUID_Parent = objOList_attribute_path.First().ID_Parent_Other,
                Type = Globals.Type_AttributeType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_attributetype_datetimestamp = (from objOItem in objDBLevel_Config1.ObjectRels
                                                    where objOItem.ID_Object == cstrID_Ontology
                                                    join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                    where objRef.Name_Object.ToLower() == "attributetype_datetimestamp".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                                    select objRef).ToList();

        if (objOList_attributetype_datetimestamp.Any())
        {
            OItem_attributetype_datetimestamp = new clsOntologyItem()
            {
                GUID = objOList_attributetype_datetimestamp.First().ID_Other,
                Name = objOList_attributetype_datetimestamp.First().Name_Other,
                GUID_Parent = objOList_attributetype_datetimestamp.First().ID_Parent_Other,
                Type = Globals.Type_AttributeType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_attributetype_pattern = (from objOItem in objDBLevel_Config1.ObjectRels
                                              where objOItem.ID_Object == cstrID_Ontology
                                              join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                              where objRef.Name_Object.ToLower() == "attributetype_pattern".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                              select objRef).ToList();

        if (objOList_attributetype_pattern.Any())
        {
            OItem_attributetype_pattern = new clsOntologyItem()
            {
                GUID = objOList_attributetype_pattern.First().ID_Other,
                Name = objOList_attributetype_pattern.First().Name_Other,
                GUID_Parent = objOList_attributetype_pattern.First().ID_Parent_Other,
                Type = Globals.Type_AttributeType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_attributetype_subitems = (from objOItem in objDBLevel_Config1.ObjectRels
                                               where objOItem.ID_Object == cstrID_Ontology
                                               join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                               where objRef.Name_Object.ToLower() == "attributetype_subitems".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                               select objRef).ToList();

        if (objOList_attributetype_subitems.Any())
        {
            OItem_attributetype_subitems = new clsOntologyItem()
            {
                GUID = objOList_attributetype_subitems.First().ID_Other,
                Name = objOList_attributetype_subitems.First().Name_Other,
                GUID_Parent = objOList_attributetype_subitems.First().ID_Parent_Other,
                Type = Globals.Type_AttributeType
            };
        }
        else
        {
            throw new Exception("config err");
        }


    }

    private void get_Config_RelationTypes()
    {
        var objOList_relationtype_authorized_by = (from objOItem in objDBLevel_Config1.ObjectRels
                                                   where objOItem.ID_Object == cstrID_Ontology
                                                   join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                   where objRef.Name_Object.ToLower() == "relationtype_authorized_by".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                   select objRef).ToList();

        if (objOList_relationtype_authorized_by.Any())
        {
            OItem_relationtype_authorized_by = new clsOntologyItem()
            {
                GUID = objOList_relationtype_authorized_by.First().ID_Other,
                Name = objOList_relationtype_authorized_by.First().Name_Other,
                GUID_Parent = objOList_relationtype_authorized_by.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_belonging = (from objOItem in objDBLevel_Config1.ObjectRels
                                               where objOItem.ID_Object == cstrID_Ontology
                                               join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                               where objRef.Name_Object.ToLower() == "relationtype_belonging".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                               select objRef).ToList();

        if (objOList_relationtype_belonging.Any())
        {
            OItem_relationtype_belonging = new clsOntologyItem()
            {
                GUID = objOList_relationtype_belonging.First().ID_Other,
                Name = objOList_relationtype_belonging.First().Name_Other,
                GUID_Parent = objOList_relationtype_belonging.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_belonging_done = (from objOItem in objDBLevel_Config1.ObjectRels
                                                    where objOItem.ID_Object == cstrID_Ontology
                                                    join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                    where objRef.Name_Object.ToLower() == "relationtype_belonging_done".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                    select objRef).ToList();

        if (objOList_relationtype_belonging_done.Any())
        {
            OItem_relationtype_belonging_done = new clsOntologyItem()
            {
                GUID = objOList_relationtype_belonging_done.First().ID_Other,
                Name = objOList_relationtype_belonging_done.First().Name_Other,
                GUID_Parent = objOList_relationtype_belonging_done.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_belonging_resource = (from objOItem in objDBLevel_Config1.ObjectRels
                                                        where objOItem.ID_Object == cstrID_Ontology
                                                        join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                        where objRef.Name_Object.ToLower() == "relationtype_belonging_resource".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                        select objRef).ToList();

        if (objOList_relationtype_belonging_resource.Any())
        {
            OItem_relationtype_belonging_resource = new clsOntologyItem()
            {
                GUID = objOList_relationtype_belonging_resource.First().ID_Other,
                Name = objOList_relationtype_belonging_resource.First().Name_Other,
                GUID_Parent = objOList_relationtype_belonging_resource.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_belonging_source = (from objOItem in objDBLevel_Config1.ObjectRels
                                                      where objOItem.ID_Object == cstrID_Ontology
                                                      join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                      where objRef.Name_Object.ToLower() == "relationtype_belonging_source".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                      select objRef).ToList();

        if (objOList_relationtype_belonging_source.Any())
        {
            OItem_relationtype_belonging_source = new clsOntologyItem()
            {
                GUID = objOList_relationtype_belonging_source.First().ID_Other,
                Name = objOList_relationtype_belonging_source.First().Name_Other,
                GUID_Parent = objOList_relationtype_belonging_source.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_belongsto = (from objOItem in objDBLevel_Config1.ObjectRels
                                               where objOItem.ID_Object == cstrID_Ontology
                                               join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                               where objRef.Name_Object.ToLower() == "relationtype_belongsto".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                               select objRef).ToList();

        if (objOList_relationtype_belongsto.Any())
        {
            OItem_relationtype_belongsto = new clsOntologyItem()
            {
                GUID = objOList_relationtype_belongsto.First().ID_Other,
                Name = objOList_relationtype_belongsto.First().Name_Other,
                GUID_Parent = objOList_relationtype_belongsto.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_connect_to = (from objOItem in objDBLevel_Config1.ObjectRels
                                                where objOItem.ID_Object == cstrID_Ontology
                                                join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                where objRef.Name_Object.ToLower() == "relationtype_connect_to".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                select objRef).ToList();

        if (objOList_relationtype_connect_to.Any())
        {
            OItem_relationtype_connect_to = new clsOntologyItem()
            {
                GUID = objOList_relationtype_connect_to.First().ID_Other,
                Name = objOList_relationtype_connect_to.First().Name_Other,
                GUID_Parent = objOList_relationtype_connect_to.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_dst = (from objOItem in objDBLevel_Config1.ObjectRels
                                         where objOItem.ID_Object == cstrID_Ontology
                                         join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                         where objRef.Name_Object.ToLower() == "relationtype_dst".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                         select objRef).ToList();

        if (objOList_relationtype_dst.Any())
        {
            OItem_relationtype_dst = new clsOntologyItem()
            {
                GUID = objOList_relationtype_dst.First().ID_Other,
                Name = objOList_relationtype_dst.First().Name_Other,
                GUID_Parent = objOList_relationtype_dst.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_ends_with = (from objOItem in objDBLevel_Config1.ObjectRels
                                               where objOItem.ID_Object == cstrID_Ontology
                                               join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                               where objRef.Name_Object.ToLower() == "relationtype_ends_with".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                               select objRef).ToList();

        if (objOList_relationtype_ends_with.Any())
        {
            OItem_relationtype_ends_with = new clsOntologyItem()
            {
                GUID = objOList_relationtype_ends_with.First().ID_Other,
                Name = objOList_relationtype_ends_with.First().Name_Other,
                GUID_Parent = objOList_relationtype_ends_with.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_fileshare = (from objOItem in objDBLevel_Config1.ObjectRels
                                               where objOItem.ID_Object == cstrID_Ontology
                                               join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                               where objRef.Name_Object.ToLower() == "relationtype_fileshare".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                               select objRef).ToList();

        if (objOList_relationtype_fileshare.Any())
        {
            OItem_relationtype_fileshare = new clsOntologyItem()
            {
                GUID = objOList_relationtype_fileshare.First().ID_Other,
                Name = objOList_relationtype_fileshare.First().Name_Other,
                GUID_Parent = objOList_relationtype_fileshare.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_is_checkout_by = (from objOItem in objDBLevel_Config1.ObjectRels
                                                    where objOItem.ID_Object == cstrID_Ontology
                                                    join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                    where objRef.Name_Object.ToLower() == "relationtype_is_checkout_by".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                    select objRef).ToList();

        if (objOList_relationtype_is_checkout_by.Any())
        {
            OItem_relationtype_is_checkout_by = new clsOntologyItem()
            {
                GUID = objOList_relationtype_is_checkout_by.First().ID_Other,
                Name = objOList_relationtype_is_checkout_by.First().Name_Other,
                GUID_Parent = objOList_relationtype_is_checkout_by.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_is_of_type = (from objOItem in objDBLevel_Config1.ObjectRels
                                                where objOItem.ID_Object == cstrID_Ontology
                                                join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                where objRef.Name_Object.ToLower() == "relationtype_is_of_type".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                select objRef).ToList();

        if (objOList_relationtype_is_of_type.Any())
        {
            OItem_relationtype_is_of_type = new clsOntologyItem()
            {
                GUID = objOList_relationtype_is_of_type.First().ID_Other,
                Name = objOList_relationtype_is_of_type.First().Name_Other,
                GUID_Parent = objOList_relationtype_is_of_type.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_isinstate = (from objOItem in objDBLevel_Config1.ObjectRels
                                               where objOItem.ID_Object == cstrID_Ontology
                                               join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                               where objRef.Name_Object.ToLower() == "relationtype_isinstate".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                               select objRef).ToList();

        if (objOList_relationtype_isinstate.Any())
        {
            OItem_relationtype_isinstate = new clsOntologyItem()
            {
                GUID = objOList_relationtype_isinstate.First().ID_Other,
                Name = objOList_relationtype_isinstate.First().Name_Other,
                GUID_Parent = objOList_relationtype_isinstate.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_issubordinated = (from objOItem in objDBLevel_Config1.ObjectRels
                                                    where objOItem.ID_Object == cstrID_Ontology
                                                    join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                    where objRef.Name_Object.ToLower() == "relationtype_issubordinated".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                    select objRef).ToList();

        if (objOList_relationtype_issubordinated.Any())
        {
            OItem_relationtype_issubordinated = new clsOntologyItem()
            {
                GUID = objOList_relationtype_issubordinated.First().ID_Other,
                Name = objOList_relationtype_issubordinated.First().Name_Other,
                GUID_Parent = objOList_relationtype_issubordinated.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_last_done = (from objOItem in objDBLevel_Config1.ObjectRels
                                               where objOItem.ID_Object == cstrID_Ontology
                                               join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                               where objRef.Name_Object.ToLower() == "relationtype_last_done".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                               select objRef).ToList();

        if (objOList_relationtype_last_done.Any())
        {
            OItem_relationtype_last_done = new clsOntologyItem()
            {
                GUID = objOList_relationtype_last_done.First().ID_Other,
                Name = objOList_relationtype_last_done.First().Name_Other,
                GUID_Parent = objOList_relationtype_last_done.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_located_in = (from objOItem in objDBLevel_Config1.ObjectRels
                                                where objOItem.ID_Object == cstrID_Ontology
                                                join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                where objRef.Name_Object.ToLower() == "relationtype_located_in".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                select objRef).ToList();

        if (objOList_relationtype_located_in.Any())
        {
            OItem_relationtype_located_in = new clsOntologyItem()
            {
                GUID = objOList_relationtype_located_in.First().ID_Other,
                Name = objOList_relationtype_located_in.First().Name_Other,
                GUID_Parent = objOList_relationtype_located_in.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_offered_by = (from objOItem in objDBLevel_Config1.ObjectRels
                                                where objOItem.ID_Object == cstrID_Ontology
                                                join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                where objRef.Name_Object.ToLower() == "relationtype_offered_by".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                select objRef).ToList();

        if (objOList_relationtype_offered_by.Any())
        {
            OItem_relationtype_offered_by = new clsOntologyItem()
            {
                GUID = objOList_relationtype_offered_by.First().ID_Other,
                Name = objOList_relationtype_offered_by.First().Name_Other,
                GUID_Parent = objOList_relationtype_offered_by.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_provides = (from objOItem in objDBLevel_Config1.ObjectRels
                                              where objOItem.ID_Object == cstrID_Ontology
                                              join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                              where objRef.Name_Object.ToLower() == "relationtype_provides".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                              select objRef).ToList();

        if (objOList_relationtype_provides.Any())
        {
            OItem_relationtype_provides = new clsOntologyItem()
            {
                GUID = objOList_relationtype_provides.First().ID_Other,
                Name = objOList_relationtype_provides.First().Name_Other,
                GUID_Parent = objOList_relationtype_provides.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_secured_by = (from objOItem in objDBLevel_Config1.ObjectRels
                                                where objOItem.ID_Object == cstrID_Ontology
                                                join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                where objRef.Name_Object.ToLower() == "relationtype_secured_by".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                select objRef).ToList();

        if (objOList_relationtype_secured_by.Any())
        {
            OItem_relationtype_secured_by = new clsOntologyItem()
            {
                GUID = objOList_relationtype_secured_by.First().ID_Other,
                Name = objOList_relationtype_secured_by.First().Name_Other,
                GUID_Parent = objOList_relationtype_secured_by.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_src = (from objOItem in objDBLevel_Config1.ObjectRels
                                         where objOItem.ID_Object == cstrID_Ontology
                                         join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                         where objRef.Name_Object.ToLower() == "relationtype_src".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                         select objRef).ToList();

        if (objOList_relationtype_src.Any())
        {
            OItem_relationtype_src = new clsOntologyItem()
            {
                GUID = objOList_relationtype_src.First().ID_Other,
                Name = objOList_relationtype_src.First().Name_Other,
                GUID_Parent = objOList_relationtype_src.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_watch = (from objOItem in objDBLevel_Config1.ObjectRels
                                           where objOItem.ID_Object == cstrID_Ontology
                                           join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                           where objRef.Name_Object.ToLower() == "relationtype_watch".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                           select objRef).ToList();

        if (objOList_relationtype_watch.Any())
        {
            OItem_relationtype_watch = new clsOntologyItem()
            {
                GUID = objOList_relationtype_watch.First().ID_Other,
                Name = objOList_relationtype_watch.First().Name_Other,
                GUID_Parent = objOList_relationtype_watch.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }


    }

    private void get_Config_Objects()
    {
        var objOList_object_blob_to_file = (from objOItem in objDBLevel_Config1.ObjectRels
                                            where objOItem.ID_Object == cstrID_Ontology
                                            join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                            where objRef.Name_Object.ToLower() == "object_blob_to_file".ToLower() && objRef.Ontology == Globals.Type_Object
                                            select objRef).ToList();

        if (objOList_object_blob_to_file.Any())
        {
            OItem_object_blob_to_file = new clsOntologyItem()
            {
                GUID = objOList_object_blob_to_file.First().ID_Other,
                Name = objOList_object_blob_to_file.First().Name_Other,
                GUID_Parent = objOList_object_blob_to_file.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_object_create = (from objOItem in objDBLevel_Config1.ObjectRels
                                      where objOItem.ID_Object == cstrID_Ontology
                                      join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                      where objRef.Name_Object.ToLower() == "object_create".ToLower() && objRef.Ontology == Globals.Type_Object
                                      select objRef).ToList();

        if (objOList_object_create.Any())
        {
            OItem_object_create = new clsOntologyItem()
            {
                GUID = objOList_object_create.First().ID_Other,
                Name = objOList_object_create.First().Name_Other,
                GUID_Parent = objOList_object_create.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_object_download = (from objOItem in objDBLevel_Config1.ObjectRels
                                        where objOItem.ID_Object == cstrID_Ontology
                                        join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                        where objRef.Name_Object.ToLower() == "object_download".ToLower() && objRef.Ontology == Globals.Type_Object
                                        select objRef).ToList();

        if (objOList_object_download.Any())
        {
            OItem_object_download = new clsOntologyItem()
            {
                GUID = objOList_object_download.First().ID_Other,
                Name = objOList_object_download.First().Name_Other,
                GUID_Parent = objOList_object_download.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_object_file_has_no_identity = (from objOItem in objDBLevel_Config1.ObjectRels
                                                    where objOItem.ID_Object == cstrID_Ontology
                                                    join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                    where objRef.Name_Object.ToLower() == "object_file_has_no_identity".ToLower() && objRef.Ontology == Globals.Type_Object
                                                    select objRef).ToList();

        if (objOList_object_file_has_no_identity.Any())
        {
            OItem_object_file_has_no_identity = new clsOntologyItem()
            {
                GUID = objOList_object_file_has_no_identity.First().ID_Other,
                Name = objOList_object_file_has_no_identity.First().Name_Other,
                GUID_Parent = objOList_object_file_has_no_identity.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_object_file_not_present = (from objOItem in objDBLevel_Config1.ObjectRels
                                                where objOItem.ID_Object == cstrID_Ontology
                                                join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                where objRef.Name_Object.ToLower() == "object_file_not_present".ToLower() && objRef.Ontology == Globals.Type_Object
                                                select objRef).ToList();

        if (objOList_object_file_not_present.Any())
        {
            OItem_object_file_not_present = new clsOntologyItem()
            {
                GUID = objOList_object_file_not_present.First().ID_Other,
                Name = objOList_object_file_not_present.First().Name_Other,
                GUID_Parent = objOList_object_file_not_present.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_object_file_to_blob = (from objOItem in objDBLevel_Config1.ObjectRels
                                            where objOItem.ID_Object == cstrID_Ontology
                                            join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                            where objRef.Name_Object.ToLower() == "object_file_to_blob".ToLower() && objRef.Ontology == Globals.Type_Object
                                            select objRef).ToList();

        if (objOList_object_file_to_blob.Any())
        {
            OItem_object_file_to_blob = new clsOntologyItem()
            {
                GUID = objOList_object_file_to_blob.First().ID_Other,
                Name = objOList_object_file_to_blob.First().Name_Other,
                GUID_Parent = objOList_object_file_to_blob.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_object_identity_is_no_guid = (from objOItem in objDBLevel_Config1.ObjectRels
                                                   where objOItem.ID_Object == cstrID_Ontology
                                                   join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                   where objRef.Name_Object.ToLower() == "object_identity_is_no_guid".ToLower() && objRef.Ontology == Globals.Type_Object
                                                   select objRef).ToList();

        if (objOList_object_identity_is_no_guid.Any())
        {
            OItem_object_identity_is_no_guid = new clsOntologyItem()
            {
                GUID = objOList_object_identity_is_no_guid.First().ID_Other,
                Name = objOList_object_identity_is_no_guid.First().Name_Other,
                GUID_Parent = objOList_object_identity_is_no_guid.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_object_identitypresent = (from objOItem in objDBLevel_Config1.ObjectRels
                                               where objOItem.ID_Object == cstrID_Ontology
                                               join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                               where objRef.Name_Object.ToLower() == "object_identitypresent".ToLower() && objRef.Ontology == Globals.Type_Object
                                               select objRef).ToList();

        if (objOList_object_identitypresent.Any())
        {
            OItem_object_identitypresent = new clsOntologyItem()
            {
                GUID = objOList_object_identitypresent.First().ID_Other,
                Name = objOList_object_identitypresent.First().Name_Other,
                GUID_Parent = objOList_object_identitypresent.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_active_server_state = (from objOItem in objDBLevel_Config1.ObjectRels
                                                  where objOItem.ID_Object == cstrID_Ontology
                                                  join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                  where objRef.Name_Object.ToLower() == "token_active_server_state".ToLower() && objRef.Ontology == Globals.Type_Object
                                                  select objRef).ToList();

        if (objOList_token_active_server_state.Any())
        {
            OItem_token_active_server_state = new clsOntologyItem()
            {
                GUID = objOList_token_active_server_state.First().ID_Other,
                Name = objOList_token_active_server_state.First().Name_Other,
                GUID_Parent = objOList_token_active_server_state.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_fileserver_server_type = (from objOItem in objDBLevel_Config1.ObjectRels
                                                     where objOItem.ID_Object == cstrID_Ontology
                                                     join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                     where objRef.Name_Object.ToLower() == "token_fileserver_server_type".ToLower() && objRef.Ontology == Globals.Type_Object
                                                     select objRef).ToList();

        if (objOList_token_fileserver_server_type.Any())
        {
            OItem_token_fileserver_server_type = new clsOntologyItem()
            {
                GUID = objOList_token_fileserver_server_type.First().ID_Other,
                Name = objOList_token_fileserver_server_type.First().Name_Other,
                GUID_Parent = objOList_token_fileserver_server_type.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_logstate_active = (from objOItem in objDBLevel_Config1.ObjectRels
                                              where objOItem.ID_Object == cstrID_Ontology
                                              join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                              where objRef.Name_Object.ToLower() == "token_logstate_active".ToLower() && objRef.Ontology == Globals.Type_Object
                                              select objRef).ToList();

        if (objOList_token_logstate_active.Any())
        {
            OItem_token_logstate_active = new clsOntologyItem()
            {
                GUID = objOList_token_logstate_active.First().ID_Other,
                Name = objOList_token_logstate_active.First().Name_Other,
                GUID_Parent = objOList_token_logstate_active.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_token_module_filesystem_management = (from objOItem in objDBLevel_Config1.ObjectRels
                                                           where objOItem.ID_Object == cstrID_Ontology
                                                           join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                           where objRef.Name_Object.ToLower() == "token_module_filesystem_management".ToLower() && objRef.Ontology == Globals.Type_Object
                                                           select objRef).ToList();

        if (objOList_token_module_filesystem_management.Any())
        {
            OItem_token_module_filesystem_management = new clsOntologyItem()
            {
                GUID = objOList_token_module_filesystem_management.First().ID_Other,
                Name = objOList_token_module_filesystem_management.First().Name_Other,
                GUID_Parent = objOList_token_module_filesystem_management.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }


    }

    private void get_Config_Classes()
    {
        var objOList_class_blobsyncdirection = (from objOItem in objDBLevel_Config1.ObjectRels
                                                where objOItem.ID_Object == cstrID_Ontology
                                                join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                where objRef.Name_Object.ToLower() == "class_blobsyncdirection".ToLower() && objRef.Ontology == Globals.Type_Class
                                                select objRef).ToList();

        if (objOList_class_blobsyncdirection.Any())
        {
            OItem_class_blobsyncdirection = new clsOntologyItem()
            {
                GUID = objOList_class_blobsyncdirection.First().ID_Other,
                Name = objOList_class_blobsyncdirection.First().Name_Other,
                GUID_Parent = objOList_class_blobsyncdirection.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_class_fileresource = (from objOItem in objDBLevel_Config1.ObjectRels
                                           where objOItem.ID_Object == cstrID_Ontology
                                           join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                           where objRef.Name_Object.ToLower() == "class_fileresource".ToLower() && objRef.Ontology == Globals.Type_Class
                                           select objRef).ToList();

        if (objOList_class_fileresource.Any())
        {
            OItem_class_fileresource = new clsOntologyItem()
            {
                GUID = objOList_class_fileresource.First().ID_Other,
                Name = objOList_class_fileresource.First().Name_Other,
                GUID_Parent = objOList_class_fileresource.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_class_filesync = (from objOItem in objDBLevel_Config1.ObjectRels
                                       where objOItem.ID_Object == cstrID_Ontology
                                       join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                       where objRef.Name_Object.ToLower() == "class_filesync".ToLower() && objRef.Ontology == Globals.Type_Class
                                       select objRef).ToList();

        if (objOList_class_filesync.Any())
        {
            OItem_class_filesync = new clsOntologyItem()
            {
                GUID = objOList_class_filesync.First().ID_Other,
                Name = objOList_class_filesync.First().Name_Other,
                GUID_Parent = objOList_class_filesync.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_class_logentry = (from objOItem in objDBLevel_Config1.ObjectRels
                                       where objOItem.ID_Object == cstrID_Ontology
                                       join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                       where objRef.Name_Object.ToLower() == "class_logentry".ToLower() && objRef.Ontology == Globals.Type_Class
                                       select objRef).ToList();

        if (objOList_class_logentry.Any())
        {
            OItem_class_logentry = new clsOntologyItem()
            {
                GUID = objOList_class_logentry.First().ID_Other,
                Name = objOList_class_logentry.First().Name_Other,
                GUID_Parent = objOList_class_logentry.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_class_logstate = (from objOItem in objDBLevel_Config1.ObjectRels
                                       where objOItem.ID_Object == cstrID_Ontology
                                       join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                       where objRef.Name_Object.ToLower() == "class_logstate".ToLower() && objRef.Ontology == Globals.Type_Class
                                       select objRef).ToList();

        if (objOList_class_logstate.Any())
        {
            OItem_class_logstate = new clsOntologyItem()
            {
                GUID = objOList_class_logstate.First().ID_Other,
                Name = objOList_class_logstate.First().Name_Other,
                GUID_Parent = objOList_class_logstate.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_class_password = (from objOItem in objDBLevel_Config1.ObjectRels
                                       where objOItem.ID_Object == cstrID_Ontology
                                       join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                       where objRef.Name_Object.ToLower() == "class_password".ToLower() && objRef.Ontology == Globals.Type_Class
                                       select objRef).ToList();

        if (objOList_class_password.Any())
        {
            OItem_class_password = new clsOntologyItem()
            {
                GUID = objOList_class_password.First().ID_Other,
                Name = objOList_class_password.First().Name_Other,
                GUID_Parent = objOList_class_password.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_class_url = (from objOItem in objDBLevel_Config1.ObjectRels
                                  where objOItem.ID_Object == cstrID_Ontology
                                  join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                  where objRef.Name_Object.ToLower() == "class_url".ToLower() && objRef.Ontology == Globals.Type_Class
                                  select objRef).ToList();

        if (objOList_class_url.Any())
        {
            OItem_class_url = new clsOntologyItem()
            {
                GUID = objOList_class_url.First().ID_Other,
                Name = objOList_class_url.First().Name_Other,
                GUID_Parent = objOList_class_url.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_class_user = (from objOItem in objDBLevel_Config1.ObjectRels
                                   where objOItem.ID_Object == cstrID_Ontology
                                   join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                   where objRef.Name_Object.ToLower() == "class_user".ToLower() && objRef.Ontology == Globals.Type_Class
                                   select objRef).ToList();

        if (objOList_class_user.Any())
        {
            OItem_class_user = new clsOntologyItem()
            {
                GUID = objOList_class_user.First().ID_Other,
                Name = objOList_class_user.First().Name_Other,
                GUID_Parent = objOList_class_user.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_class_user_authentication = (from objOItem in objDBLevel_Config1.ObjectRels
                                                  where objOItem.ID_Object == cstrID_Ontology
                                                  join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                  where objRef.Name_Object.ToLower() == "class_user_authentication".ToLower() && objRef.Ontology == Globals.Type_Class
                                                  select objRef).ToList();

        if (objOList_class_user_authentication.Any())
        {
            OItem_class_user_authentication = new clsOntologyItem()
            {
                GUID = objOList_class_user_authentication.First().ID_Other,
                Name = objOList_class_user_authentication.First().Name_Other,
                GUID_Parent = objOList_class_user_authentication.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_class_web_connection = (from objOItem in objDBLevel_Config1.ObjectRels
                                             where objOItem.ID_Object == cstrID_Ontology
                                             join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                             where objRef.Name_Object.ToLower() == "class_web_connection".ToLower() && objRef.Ontology == Globals.Type_Class
                                             select objRef).ToList();

        if (objOList_class_web_connection.Any())
        {
            OItem_class_web_connection = new clsOntologyItem()
            {
                GUID = objOList_class_web_connection.First().ID_Other,
                Name = objOList_class_web_connection.First().Name_Other,
                GUID_Parent = objOList_class_web_connection.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_blobdirwatcher = (from objOItem in objDBLevel_Config1.ObjectRels
                                            where objOItem.ID_Object == cstrID_Ontology
                                            join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                            where objRef.Name_Object.ToLower() == "type_blobdirwatcher".ToLower() && objRef.Ontology == Globals.Type_Class
                                            select objRef).ToList();

        if (objOList_type_blobdirwatcher.Any())
        {
            OItem_type_blobdirwatcher = new clsOntologyItem()
            {
                GUID = objOList_type_blobdirwatcher.First().ID_Other,
                Name = objOList_type_blobdirwatcher.First().Name_Other,
                GUID_Parent = objOList_type_blobdirwatcher.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_database = (from objOItem in objDBLevel_Config1.ObjectRels
                                      where objOItem.ID_Object == cstrID_Ontology
                                      join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                      where objRef.Name_Object.ToLower() == "type_database".ToLower() && objRef.Ontology == Globals.Type_Class
                                      select objRef).ToList();

        if (objOList_type_database.Any())
        {
            OItem_type_database = new clsOntologyItem()
            {
                GUID = objOList_type_database.First().ID_Other,
                Name = objOList_type_database.First().Name_Other,
                GUID_Parent = objOList_type_database.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_database_on_server = (from objOItem in objDBLevel_Config1.ObjectRels
                                                where objOItem.ID_Object == cstrID_Ontology
                                                join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                where objRef.Name_Object.ToLower() == "type_database_on_server".ToLower() && objRef.Ontology == Globals.Type_Class
                                                select objRef).ToList();

        if (objOList_type_database_on_server.Any())
        {
            OItem_type_database_on_server = new clsOntologyItem()
            {
                GUID = objOList_type_database_on_server.First().ID_Other,
                Name = objOList_type_database_on_server.First().Name_Other,
                GUID_Parent = objOList_type_database_on_server.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_drive = (from objOItem in objDBLevel_Config1.ObjectRels
                                   where objOItem.ID_Object == cstrID_Ontology
                                   join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                   where objRef.Name_Object.ToLower() == "type_drive".ToLower() && objRef.Ontology == Globals.Type_Class
                                   select objRef).ToList();

        if (objOList_type_drive.Any())
        {
            OItem_type_drive = new clsOntologyItem()
            {
                GUID = objOList_type_drive.First().ID_Other,
                Name = objOList_type_drive.First().Name_Other,
                GUID_Parent = objOList_type_drive.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_extensions = (from objOItem in objDBLevel_Config1.ObjectRels
                                        where objOItem.ID_Object == cstrID_Ontology
                                        join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                        where objRef.Name_Object.ToLower() == "type_extensions".ToLower() && objRef.Ontology == Globals.Type_Class
                                        select objRef).ToList();

        if (objOList_type_extensions.Any())
        {
            OItem_type_extensions = new clsOntologyItem()
            {
                GUID = objOList_type_extensions.First().ID_Other,
                Name = objOList_type_extensions.First().Name_Other,
                GUID_Parent = objOList_type_extensions.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_file = (from objOItem in objDBLevel_Config1.ObjectRels
                                  where objOItem.ID_Object == cstrID_Ontology
                                  join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                  where objRef.Name_Object.ToLower() == "type_file".ToLower() && objRef.Ontology == Globals.Type_Class
                                  select objRef).ToList();

        if (objOList_type_file.Any())
        {
            OItem_type_file = new clsOntologyItem()
            {
                GUID = objOList_type_file.First().ID_Other,
                Name = objOList_type_file.First().Name_Other,
                GUID_Parent = objOList_type_file.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_filesystem_management = (from objOItem in objDBLevel_Config1.ObjectRels
                                                   where objOItem.ID_Object == cstrID_Ontology
                                                   join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                   where objRef.Name_Object.ToLower() == "type_filesystem_management".ToLower() && objRef.Ontology == Globals.Type_Class
                                                   select objRef).ToList();

        if (objOList_type_filesystem_management.Any())
        {
            OItem_type_filesystem_management = new clsOntologyItem()
            {
                GUID = objOList_type_filesystem_management.First().ID_Other,
                Name = objOList_type_filesystem_management.First().Name_Other,
                GUID_Parent = objOList_type_filesystem_management.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_folder = (from objOItem in objDBLevel_Config1.ObjectRels
                                    where objOItem.ID_Object == cstrID_Ontology
                                    join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                    where objRef.Name_Object.ToLower() == "type_folder".ToLower() && objRef.Ontology == Globals.Type_Class
                                    select objRef).ToList();

        if (objOList_type_folder.Any())
        {
            OItem_type_folder = new clsOntologyItem()
            {
                GUID = objOList_type_folder.First().ID_Other,
                Name = objOList_type_folder.First().Name_Other,
                GUID_Parent = objOList_type_folder.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_module = (from objOItem in objDBLevel_Config1.ObjectRels
                                    where objOItem.ID_Object == cstrID_Ontology
                                    join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                    where objRef.Name_Object.ToLower() == "type_module".ToLower() && objRef.Ontology == Globals.Type_Class
                                    select objRef).ToList();

        if (objOList_type_module.Any())
        {
            OItem_type_module = new clsOntologyItem()
            {
                GUID = objOList_type_module.First().ID_Other,
                Name = objOList_type_module.First().Name_Other,
                GUID_Parent = objOList_type_module.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_path = (from objOItem in objDBLevel_Config1.ObjectRels
                                  where objOItem.ID_Object == cstrID_Ontology
                                  join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                  where objRef.Name_Object.ToLower() == "type_path".ToLower() && objRef.Ontology == Globals.Type_Class
                                  select objRef).ToList();

        if (objOList_type_path.Any())
        {
            OItem_type_path = new clsOntologyItem()
            {
                GUID = objOList_type_path.First().ID_Other,
                Name = objOList_type_path.First().Name_Other,
                GUID_Parent = objOList_type_path.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_server = (from objOItem in objDBLevel_Config1.ObjectRels
                                    where objOItem.ID_Object == cstrID_Ontology
                                    join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                    where objRef.Name_Object.ToLower() == "type_server".ToLower() && objRef.Ontology == Globals.Type_Class
                                    select objRef).ToList();

        if (objOList_type_server.Any())
        {
            OItem_type_server = new clsOntologyItem()
            {
                GUID = objOList_type_server.First().ID_Other,
                Name = objOList_type_server.First().Name_Other,
                GUID_Parent = objOList_type_server.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_server_state = (from objOItem in objDBLevel_Config1.ObjectRels
                                          where objOItem.ID_Object == cstrID_Ontology
                                          join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                          where objRef.Name_Object.ToLower() == "type_server_state".ToLower() && objRef.Ontology == Globals.Type_Class
                                          select objRef).ToList();

        if (objOList_type_server_state.Any())
        {
            OItem_type_server_state = new clsOntologyItem()
            {
                GUID = objOList_type_server_state.First().ID_Other,
                Name = objOList_type_server_state.First().Name_Other,
                GUID_Parent = objOList_type_server_state.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_type_server_type = (from objOItem in objDBLevel_Config1.ObjectRels
                                         where objOItem.ID_Object == cstrID_Ontology
                                         join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                         where objRef.Name_Object.ToLower() == "type_server_type".ToLower() && objRef.Ontology == Globals.Type_Class
                                         select objRef).ToList();

        if (objOList_type_server_type.Any())
        {
            OItem_type_server_type = new clsOntologyItem()
            {
                GUID = objOList_type_server_type.First().ID_Other,
                Name = objOList_type_server_type.First().Name_Other,
                GUID_Parent = objOList_type_server_type.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }


    }

        public string IdLocalConfig
        {
            get
            {
                var attrib =
                      Assembly.GetExecutingAssembly()
                          .GetCustomAttributes(true)
                          .FirstOrDefault(objAttribute => objAttribute is GuidAttribute);
                if (attrib != null)
                {
                    return ((GuidAttribute)attrib).Value;
                }
                else
                {
                    return null;
                }
            }
        }
    }

}